import { Button, FormControl, FormControlLabel, List, ListItem, Radio, RadioGroup, Typography } from '@material-ui/core';
import Cookies from 'js-cookie';
import { useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react'
import CheckoutWizard from '../components/CheckoutWizard'
import Layout from '../components/Layout'
import { Store } from '../utils/store';
import useStyles from '../utils/styles'

export default function Payment() {
  const classes = useStyles()
  const router = useRouter();

  const { state, dispatch } = useContext(Store)
  const { cart: { shippingAddress ,paymentMethod:payment } } = state;
  console.log(shippingAddress, payment)

  const [paymentMethod, setPaymentMethod] = useState('')

  useEffect(() => {
    (() => {
      if (!shippingAddress.address) {
        router.push('/shipping')
      } else {
        if(!payment) return
        let paymentMethod = JSON.parse(Cookies.get('paymentMethod')) || ''
        setPaymentMethod(paymentMethod)
        console.log("payment", paymentMethod)

      }
    })()
  }, []);

  const submitHandler = (e) => {
    e.preventDefault();
    if (!paymentMethod) {
      alert('Payment method is required')
    } else {
      dispatch({ type: 'SAVE_PAYMENT_METHOD', payload: paymentMethod })
      router.push('/placeorder');
    }
  }
  return (
    <div>
      <Layout title="Payment Method">
        <CheckoutWizard activeStep={2} />
        <form className={classes.form} onSubmit={submitHandler}>
          <Typography component="h1" variant="h1">Payment Method</Typography>
          <List>
            <ListItem>
              <FormControl component="fieldset">
                <RadioGroup
                  aria-label="Payment Method"
                  name="paymentMethod"
                  value={paymentMethod}
                  onChange={e => setPaymentMethod(e.target.value)}
                >
                  <FormControlLabel
                    label="PayPal"
                    value="PayPal"
                    control={<Radio />}
                  ></FormControlLabel>
                  <FormControlLabel
                    label="Stripe"
                    value="Stripe"
                    control={<Radio />}
                  ></FormControlLabel>
                  <FormControlLabel
                    label="Cash"
                    value="Cash"
                    control={<Radio />}
                  ></FormControlLabel>
                </RadioGroup>
              </FormControl>
            </ListItem>
            <ListItem>
              <Button
                variant="contained"
                type="submit"
                fullWidth
                color="primary"
              >Continue</Button>
            </ListItem>
            <ListItem>
              <Button
                variant="contained"
                type="button"
                fullWidth
                color=""
                onClick={() => router.push('/shipping')}
              >Back</Button>
            </ListItem>
          </List>
        </form>
      </Layout>
    </div>
  )
}
