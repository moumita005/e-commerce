import { Button, Link, List, ListItem, TextField, Typography } from '@material-ui/core'
import React, { useContext, useState, useEffect } from 'react'
import NextLink from 'next/link'
import Layout from '../components/Layout'
import useStyles from '../utils/styles'
import axios from 'axios'
import { Store } from '../utils/store'
import { useRouter } from 'next/router'
import Cookies from 'js-cookie'
import { Controller, useForm } from 'react-hook-form'
// import { useSnackbar } from 'notistack'

export default function Login() {
  const { handleSubmit, control, formState: { errors } } = useForm();
  // const {enqueueSnackbar, closeSnackbar } = useSnackbar();
  const router = useRouter();
  const { redirect } = router.query
  const { state, dispatch } = useContext(Store);
  const { userInfo } = state;
  useEffect(() => {
    return (() => {
      if (userInfo) {
        router.push('/')
      }
    })()
  }, [])


  // const [email, setEmail] = useState('')
  // const [password, setPassword] = useState('')
  const classes = useStyles();
  const submitHandler = async ({ email, password }) => {
    try {
      const { data } = await axios.post('/api/users/login', { email, password })
      console.log("data", data)
      dispatch({ type: "USER_LOGIN", payload: data })
      // Cookies.set('userInfo', data);
      router.push(redirect || '/')
      alert("logged in sucessfully")
    } catch (err) {
      // enqueueSnackbar(err.response.data ? err.response.data.message : err.message, {variant:'error'})
      alert(err.response.data ? err.response.data.message : err.message)
    }
  }
  return (
    <div>
      <Layout title="Login">
        <form className={classes.form} onSubmit={handleSubmit(submitHandler)}>
          <Typography component="h1" variant="h1">
            Login
          </Typography>
          <List>
            <ListItem>
              <Controller
                name="email"
                control={control}
                defaultValue=""
                rules={{
                  required: true,
                  pattern: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/

                }}
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="email"
                    name="email"
                    label="Email"
                    inputProps={{ type: "email" }}
                    error={Boolean(errors.email)}
                    helperText={errors.email ? errors.email.type === 'pattern' ? 'Email is not valid' : 'Email is required' : ''}
                    // onChange={e => setEmail(e.target.value)}
                    {...field}
                  ></TextField>
                )}>
              </Controller>

            </ListItem>
            <ListItem>
              <Controller
                name="password"
                control={control}
                defaultValue=""
                rules={{
                  required: true,
                  minLength:6
                }}
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="password"
                    name="password"
                    label="Password"
                    inputProps={{ type: "password" }}
                    error={Boolean(errors.password)}
                    helperText={errors.password ? errors.password.type === 'minLength' ? 'Password length is more than 5' : 'Password is required' : ''}
                    // onChange={e => setEmail(e.target.value)}
                    {...field}
                  ></TextField>
                )}>
              </Controller>

            </ListItem>
            <ListItem>
              <Button
                variant="contained"
                type="submit"
                fullWidth
                color="primary"
              >LOGIN</Button>
            </ListItem>
            <ListItem>
              Don't have an account? &nbsp; <NextLink href={`/register?redirect=${redirect || '/'}`} passHref><Link>Register</Link></NextLink>
            </ListItem>
          </List>
        </form>
      </Layout>
    </div>
  )
}
