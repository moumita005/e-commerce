import {
  Button,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Typography
} from '@material-ui/core'
import Layout from '../components/Layout'
import styles from '../styles/Home.module.css'
import data from '../utils/data'
import NextLink from 'next/link'
import db from '../utils/db'
import Product from '../models/Product'
import axios from 'axios'
import { useContext } from 'react'
import { Store } from '../utils/store'
import router, { useRouter } from 'next/router'

export default function Home(props) {
  const {state, dispatch} = useContext(Store);
  const {cart: {cartItems}} = state
  const router = useRouter()
  const {products} = props
  let prod;

  const addToCartHAndler = async(product) => {
    
    const existItem = state.cart.cartItems.find(x=> x._id === product._id)
    const quantity  = existItem? existItem.quantity+1 : 1
    const {data} = await axios.get(`/api/products/${product._id}`)
    if(data.countInStock<= quantity) {
      window.alert( "Sorry. The Product is out of stock")
      quantity=existItem.quantity
    }  
    prod = cartItems.find(x=> x._id === product._id)
    dispatch({type: 'CART_ADD_ITEM', payload: {...product, quantity}})
    // router.push('/cart')
    console.log(prod)
  }
  return (
    <Layout>
      <div>
        <h1>Product</h1>

        <Grid container spacing={3}>
          {
            products.map((item) => {
              return (
                <Grid item md={4} key={item._id}>
                  <Card>
                    <NextLink href={`/product/${item.slug}`} passHref>
                      <CardActionArea>
                        <CardMedia component="img" image={item.image} title={item.name}></CardMedia>
                        <CardContent>
                          <Typography>
                            {item.name}
                          </Typography>
                        </CardContent>
                      </CardActionArea>

                    </NextLink>
                    <CardActions>
                      <Typography>${item.price}</Typography>
                      <Button size="small" color="primary" onClick={()=>addToCartHAndler(item)}>Add to Cart</Button>
                    </CardActions>
                  </Card>
                </Grid>)
            })
          }
        </Grid>

      </div>

    </Layout>
  )
}


export async function getServerSideProps(){
  await db.connect();
  const products = await Product.find({}).lean();
  await db.disconnect();
  return {
    props: {
      products: products.map(db.convertDocToObj)
    }
  }
}
