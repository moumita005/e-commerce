import { Button, Link, List, ListItem, TextField, Typography } from '@material-ui/core'
import React, { useContext, useState, useEffect } from 'react'
import NextLink from 'next/link'
import Layout from '../components/Layout'
import useStyles from '../utils/styles'
import axios from 'axios'
import { Store } from '../utils/store'
import { useRouter } from 'next/router'
import Cookies from 'js-cookie'
import { Controller, useForm } from 'react-hook-form'

export default function Register() {
  const { handleSubmit, control, formState: { errors } } = useForm();
  const router = useRouter();
  const { redirect } = router.query
  const { state, dispatch } = useContext(Store);
  const { userInfo } = state;
  useEffect(() => {
    return (() => {
      if (userInfo) {
        router.push('/')
      }
    })()
  }, [])


  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmpassword, setConfirmPassword] = useState('')
  const classes = useStyles();
  const submitHandler = async ( { name, email, password, confirmPassword }) => {
    // e.preventDefault();
    if (password !== confirmPassword) {
      alert("passwords don't match");
      return;
    }
    try {
      const { data } = await axios.post('/api/users/register', { name, email, password })
      console.log("data", data)
      dispatch({ type: "USER_LOGIN", payload: data })
      Cookies.set('userInfo', data);
      router.push(redirect || '/')

    } catch (err) {
      alert(err.response.data ? err.response.data.message : err.message)
    }
  }
  return (
    <div>
      <Layout title="Register">
        <form className={classes.form} onSubmit={handleSubmit(submitHandler)}>
          <Typography component="h1" variant="h1">
            Register
          </Typography>
          <List>
            <ListItem>
            <Controller
                name="name"
                control={control}
                defaultValue=""
                rules={{
                  required: true,
                  minLength:2
                }}
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="name"
                    name="name"
                    label="Name"
                    inputProps={{ type: "text" }}
                    error={Boolean(errors.name)}
                    helperText={errors.name ? errors.name.type === 'minLength' ? 'Name length is more than 1' : 'Name is required' : ''}
                    // onChange={e => setEmail(e.target.value)}
                    {...field}
                  ></TextField>
                )}>
              </Controller>
            </ListItem>
            <ListItem>
              <Controller
                name="email"
                control={control}
                defaultValue=""
                rules={{
                  required: true,
                  pattern: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/

                }}
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="email"
                    name="email"
                    label="Email"
                    inputProps={{ type: "email" }}
                    error={Boolean(errors.email)}
                    helperText={errors.email ? errors.email.type === 'pattern' ? 'Email is not valid' : 'Email is required' : ''}
                    // onChange={e => setEmail(e.target.value)}
                    {...field}
                  ></TextField>
                )}>
              </Controller>
            </ListItem>
            <ListItem>
              <Controller
                name="password"
                control={control}
                defaultValue=""
                rules={{
                  required: true,
                  minLength: 6
                }}
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="password"
                    name="password"
                    label="Password"
                    inputProps={{ type: "password" }}
                    error={Boolean(errors.password)}
                    helperText={errors.password ? errors.password.type === 'minLength' ? 'Password length is more than 5' : 'Password is required' : ''}
                    // onChange={e => setEmail(e.target.value)}
                    {...field}
                  ></TextField>
                )}>
              </Controller>
            </ListItem>
            <ListItem>
            <Controller
                name="confirmPassword"
                control={control}
                defaultValue=""
                rules={{
                  required: true,
                  minLength: 6
                }}
                render={({ field }) => (
                  <TextField
                    variant="outlined"
                    fullWidth
                    id="confirmPassword"
                    name="confirmPassword"
                    label="Confirm Password"
                    inputProps={{ type: "password" }}
                    error={Boolean(errors.confirmPassword)}
                    helperText={errors.confirmPassword ? errors.confirmPassword.type === 'minLength' ? 'Confirm Password length is more than 5' : 'Confirm Password is required' : ''}
                    // onChange={e => setEmail(e.target.value)}
                    {...field}
                  ></TextField>
                )}>
              </Controller>
            </ListItem>
            <ListItem>
              <Button
                variant="contained"
                type="submit"
                fullWidth
                color="primary"
              >Register</Button>
            </ListItem>
            <ListItem>
              Already have an account? &nbsp; <NextLink href={`/login?redirect=${redirect || '/'}`} passHref><Link>Login</Link></NextLink>
            </ListItem>
          </List>
        </form>
      </Layout>
    </div>
  )
}
