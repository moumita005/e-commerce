import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  navbar: {
    backgroundColor:'#203040',
    '& a':{
      color: "#fffff",
      marginLeft:10
    }
  },
  brand: {
    fontWeight: 'bold',
    fontSize:'2em'
  },
  grow:{
    flexGrow: 1
  },
  main:{
    minHeight:'80vh',
  },

  footer: {
    marginTop:10,
    textAlign: 'center'
  },
  section:{
    marginTop:10,
    marginBottom:10,
  },
  form: {
    maxWidth: 800,
    margin: '0 auto'
  },
  navbarButton: {
    color: '#ffff',
    textTransform:'initial'
  },
  transparentBackground: {
    backgroundColor: 'transparent'
  },
  error: {
    color: '#404040'
  }
})

export default useStyles;