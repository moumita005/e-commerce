import React, { useContext, useState } from 'react'
import Head from 'next/head'
import NextLink from 'next/link'
import { AppBar, Badge, Button, Container, createTheme, CssBaseline, Link, Menu, MenuItem, Switch, Toolbar, Typography } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles';
import useStyles from '../utils/styles';
import { Store } from '../utils/store';
import Cookies from 'js-cookie';
import { useRouter } from 'next/router';

export default function Layout({ title, description, children }) {
  const [anchorEl, setAnchorEl] = useState(null)
  const router = useRouter()
  const { state, dispatch } = useContext(Store);
  const { darkMode, cart, userInfo } = state
  const theme = createTheme({
    typography: {
      h1: {
        fontSize: '1.6rem',
        fontWeight: 400,
        margin: '1rem 0'
      },
      h2: {
        fontSize: '1.2rem',
        fontWeight: 400,
        margin: '1rem 0'
      },
    },
    palette: {
      type: darkMode ? 'dark' : 'light',
      primary: {
        main: '#f0c000'
      },
      secondary: {
        main: "#208080"
      }
    }
  })
  const classes = useStyles()
  const darkModeChange = () => {
    dispatch({ type: darkMode ? 'DARK_MODE_OFF' : 'DARK_MODE_ON' })
    const newDarkMode = !darkMode;
    Cookies.set('darkMode', newDarkMode ? 'ON' : 'OFF')
  }

  const loginClickHandler = (e) =>{
    setAnchorEl(e.currentTarget)
  }

  const loginMenuCloseHandler = (e, redirect) => {
    setAnchorEl(null);
    if(redirect){
      router.push(redirect)
    }
  }
  const logoutClickHandler = (e) => {
    setAnchorEl(null);
    dispatch({type: 'USER_LOGOUT'});
    Cookies.remove('userInfo')
    Cookies.remove('cartItems');
    router.push('/')
  }
  return (
    <div>
      <Head>
        <title>{title ? `${title} | Next Amazona` : "Next Amazona"} </title>
        {description && <meta name="description" content={description}></meta>}
      </Head>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <AppBar position="static" className={classes.navbar}>
          <Toolbar>
            <NextLink href="/" passHref>
              <Link>
                <Typography className={classes.brand} style={{ color: "#ffffff" }}>amazona</Typography>
              </Link>
            </NextLink>
            <div className={classes.grow}></div>
            <div>
              <Switch checked={darkMode} onChange={darkModeChange}></Switch>
              <NextLink href="/cart" passHref>
                <Link style={{ color: "#ffffff" }}>
                  {cart.cartItems.length > 0 ? <Badge color="secondary" badgeContent={cart.cartItems.length}>Cart</Badge> : 'Cart'}
                </Link>
              </NextLink>
              {userInfo ?(
                <> 
                  <Button
                    aria-controls="simple-menu"
                    aria-haspopup="true"
                    onClick={loginClickHandler}
                    className={classes.navbarButton}>
                    {userInfo?.name}
                  </Button> 
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={loginMenuCloseHandler}
                  >
                     <MenuItem onClick={(e)=>loginMenuCloseHandler(e, '/profile')}>Profile</MenuItem>
                     {/* <MenuItem onClick={(e)=>loginMenuCloseHandler(e, '/order-history')}>Order history</MenuItem> */}
                     <MenuItem onClick={logoutClickHandler}>Log out</MenuItem>
                  </Menu>
                </>
                ) :(
                <NextLink href="/login" passHref>
                  <Link style={{ color: "#ffffff" }}>
                    Login
                  </Link>
                </NextLink>)}

            </div>
          </Toolbar>
        </AppBar>
        <Container className={classes.main}>
          {children}
        </Container>
        <footer className={classes.footer}>
          <Typography>
            All rights reserved. Next Amazona
          </Typography>
        </footer>
      </ThemeProvider>

    </div>
  )
}
